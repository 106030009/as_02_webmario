// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html



const {ccclass, property} = cc._decorator;

@ccclass
export default class Enemy_goomba extends cc.Component {


    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState
    
    private rebornPos = null;

    private isDead = true;

    private speed = 100;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    start() {
        this.rebornPos = this.node.position;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0);
        this.isDead = false;
    }

    update(dt) {
        if(this.isDead) {
            this.resetPos();
            this.isDead = false;
        }
    }

    public resetPos() {
        this.node.position = this.rebornPos;
        this.node.scaleX = 3;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0);
    }

    onBeginContact(contact, self, other) {
        if(other.node.name == "left_bound") {
            this.node.scaleX = 3;    // turn right.
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(100, 0);
        } else if(other.node.name == "right_bound") {
            this.node.scaleX = -3;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
        }
        else if(other.tag == 3){ // block.
            this.speed = -this.speed;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0); 
                // if(this.node.getComponent(cc.RigidBody).linearVelocity == cc.v2(100, 0)){ //Mow moving right.
                //     this.node.scaleX =  -3; // Turn Left.
                //     this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0); // Move left.
                // }
                // else{
                //     this.node.scaleX = 3;
                //     this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(100, 0);
                // }

        }else if(other.node.name =="mario"  || other.node.name=="mario_level2"){
            if(contact.getWorldManifold().normal.y==1){
                this.animateState = this.anim.play('goomba_shrink');
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);     
                setTimeout(()=>{
                         this.node.destroy();
                        //  this.rebornPos();
                     }, 200);
            }
            else{
                // mario dead.
            }
        }
    } // end onBeginContact.



}


