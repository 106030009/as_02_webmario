// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class setting_mode extends cc.Component {


    Onep(event, customEvent){
        cc.log(customEvent);
        cc.director.loadScene("setting");

    }
    
    Twop(event, customEvent){
        cc.log(customEvent);
        cc.director.loadScene("setting_2player");

    }

    Back(event, customEvent){
        cc.log(customEvent);
        cc.director.loadScene("main");

    }



    Button_init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "setting_mode";
        button_Act1.handler = "Onep";
        button_Act1.customEventData = "Load level one.";

        cc.find("Canvas/Onepbtn").getComponent(cc.Button).clickEvents.push(button_Act1);

        let button_Act2 = new cc.Component.EventHandler();
        button_Act2.target = this.node;
        button_Act2.component = "setting_mode";
        button_Act2.handler = "Twop";
        button_Act2.customEventData = "Load level two.";

        cc.find("Canvas/Twopbtn").getComponent(cc.Button).clickEvents.push(button_Act2);

        let button_Act3 = new cc.Component.EventHandler();
        button_Act3.target = this.node;
        button_Act3.component = "setting_mode";
        button_Act3.handler = "Back";
        button_Act3.customEventData = "Back";

        cc.find("Canvas/Backbtn").getComponent(cc.Button).clickEvents.push(button_Act3);


    }

    start () {
        this.Button_init();
    }
}
