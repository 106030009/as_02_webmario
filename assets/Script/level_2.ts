// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import Player from "./Player";
const {ccclass, property} = cc._decorator;

@ccclass
export default class level_2 extends cc.Component {

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip[]= [];

    @property({type:cc.AudioClip})
    victory: cc.AudioClip = null;

    // remain_lives: number = 0;
    levelText: cc.Label;
    timeText: cc.Label;

    gameLevel = 2;
    remainTime = 300; // 300 second

    newscore: number = 0;
    gameover: boolean = false;
    
    initProperties(){
        this.levelText = cc.find("Canvas/Main Camera/level2/TopContent/Level_two/level").getComponent(cc.Label);
        this.timeText = cc.find("Canvas/Main Camera/level2/TopContent/Time/time").getComponent(cc.Label);
    }


    updateUI(dt){
        this.levelText.string = this.gameLevel.toString();

        this.remainTime -= dt;
        if(this.remainTime < 0){
            this.remainTime = 0;
        }
        // this.timeText.string = this.remainTime.toString();
        this.timeText.string = this.remainTime.toFixed(0).toString().replace(".", ":");
    }


    private count : number = 0;

    playBGM(index: number){
        // ===================== TODO =====================
        // 1. Play music. The audio clip to play is this.bgm
        cc.audioEngine.playMusic(this.bgm[index], true)
        // ================================================

    }

    stopBGM(){
        // ===================== TODO =====================
        // 1. Stop music. 
        cc.audioEngine.pauseMusic();
        // ================================================
    }
    resumeBGM(){
        cc.audioEngine.resumeMusic();
    }

    playEffect(){
        // ===================== TODO =====================
        // 1. Play sound effect. The audio clip to play is 
        cc.log("Click.");
        // cc.audioEngine.playEffect(this.touch_coin, false);
        // ================================================
    }

    Toggle_init(){
        let checkEventHandler = new cc.Component.EventHandler();
        checkEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        checkEventHandler.component = "level_2";
        checkEventHandler.handler = "Pause";
        checkEventHandler.customEventData = "foobar";
        cc.find("Canvas/Main Camera/level2/UI/Pausebtn").getComponent(cc.Button).clickEvents.push(checkEventHandler);
        // this.toggle.checkEvents.push(checkEventHandler);
    }
 
    Pause(event, customEventData) {
            // 这里的 toggle 是事件发出的 Toggle 组件
            // 这里的 customEventData 参数就等于之前设置的 "foobar"
            this.count ++;
            if(this.count>1){
                this.count = 0;
                this.resumeBGM();
            }
            else{
                cc.log("Pause");
                this.stopBGM();
            }
    }

    win(){

        cc.log("win");
        this.stopBGM();
        cc.audioEngine.playMusic(this.victory, false);

        var user = firebase.auth().currentUser;
        // Update total scores.

        firebase.database().ref('Users/'+user.displayName+'/num_of_times').once('value',(snapshot)=>{
            // get Level 1 scores.
            var num_of_times = snapshot.val();
            firebase.database().ref('Users/'+user.displayName+ '/'+num_of_times).once('value',(snapshot)=>{
                
                let score1: number = Number(snapshot.child('scores').val());
                let time1: number = snapshot.child('time').val();
                this.newscore = Number(score1 + Number(cc.find("Canvas/mario").getComponent("Player_level2").score));
                let time2 = 300 - +this.remainTime.toFixed(0);
                let newtime :number = time1 + time2;

                cc.log("newScore : " +this.newscore+", newTime : " +newtime);

                firebase.database().ref('Users/'+user.displayName+'/'+num_of_times).update({
                    scores : this.newscore,
                    time: newtime
                }).then(()=>{

                    // Check if need to update 'Best_score'
                    firebase.database().ref('Users/'+user.displayName+'/Best_score').once('value',(snapshot)=>{
                        // get current times.
                        if( this.newscore > snapshot.val()){
                            // Update Best_score.
                            alert("New record! Score: "+ this.newscore);
                            firebase.database().ref('Users/'+user.displayName+'/').update({
                                Best_score: this.newscore
                            })
                        }
                        
                    })  // end update Best_scores.
                    
                }).catch((err)=>{
                    alert("Update: " +err);

                })
            }).catch((err)=>{
                alert("Read this time scores : " +err);
            })
        }).catch((err)=>{
            alert("Cannot read 'num_of_times': "+err);
        })



        // Update user lives.
        firebase.database().ref('Users/'+user.displayName+'/').update({
            Lives: cc.find("Canvas/mario_level2").getComponent("Player_level2").lives
        })




        setTimeout(()=>{
            cc.director.loadScene("Victory");
            return;
        }, 5000);
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initProperties();
        let index = this.randomGenBGM();
        this.playBGM(index);
        this.gameover = false;  //initialize.
        
    }

    start () {
        this.Toggle_init();
    }

    update (dt) {
        if(!this.gameover){
            this.updateUI(dt);
            if(this.remainTime == 0){
                this.gameover = true;
                this.onGameover();
            }
        }
    }
    randomGenBGM()
    {
        let rand = Math.random();

        //0: bgm1, 1: bgm2, 2: bgm3
        let prob = [1,1,1];
        let sum = prob.reduce((a,b)=>a+b);

        for(let i = 1; i < prob.length; i++)
            prob[i] += prob[i-1];
        for(let i = 0; i < prob.length; i++)
        {
            prob[i] /= sum;
            if(rand <= prob[i])
                return i;
        }
    }


    onGameover(){
        cc.director.loadScene("Gameover");
    }


}
