// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class One2Two extends cc.Component {


    // onLoad () {}


    Button_init(){
        let Startbtn = new cc.Component.EventHandler();
        Startbtn.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        Startbtn .component = "One2Two";
        Startbtn .handler = "GO_to_level2";
        Startbtn .customEventData = "foobar";
        cc.find("Canvas/Background/Startbtn").getComponent(cc.Button).clickEvents.push(Startbtn);

        let Exitbtn = new cc.Component.EventHandler();
        Exitbtn.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        Exitbtn .component = "One2Two";
        Exitbtn .handler = "EXIT";
        Exitbtn .customEventData = "foobar";
        cc.find("Canvas/Background/Exitbtn").getComponent(cc.Button).clickEvents.push(Exitbtn);

    }
 
    GO_to_level2(event, customEventData) {
        setTimeout(()=>{
            cc.director.loadScene("level_2");
            return;
        }, 1000);        

    }

    EXIT(event, customEventData) {
        setTimeout(()=>{
            cc.director.loadScene("main");
            return;
        }, 1000);
    }


    onLoad() {
        this.Button_init();
    }

    // update (dt) {}
}
