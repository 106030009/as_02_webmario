// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Qblock extends cc.Component {

    beenhit: boolean = false;   //init value

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
    }

    start () {

    }

    onBeginContact(contact, self, other){
                // QBlock is Top.
                if(contact.getWorldManifold().normal.y== -1){
                    if(other.node.name == "mario") {
                        this.beenhit = true;
                    }else{
                        cc.log("Qblock been hit by something.");
                        // do nothing.
                    }
                  }
    }

    // update (dt) {}
}
