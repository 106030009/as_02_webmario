// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Victory extends cc.Component {



    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;   // we round the world

    NameText: cc.Label;
    numText: cc.Label;
    scoreText: cc.Label;
    timeText: cc.Label;
    BestscoreText: cc.Label;

    
    initProperties(){
        this.NameText = cc.find("Canvas/Background/Container/Content/Record/Username").getComponent(cc.Label);
        this.numText = cc.find("Canvas/Background/Container/Content/Record/num_of_times").getComponent(cc.Label);
        this.scoreText = cc.find("Canvas/Background/Container/Content/Score/score").getComponent(cc.Label);
        this.timeText = cc.find("Canvas/Background/Container/Content/Time/time").getComponent(cc.Label);
        this.BestscoreText = cc.find("Canvas/Background/Container/Content/Best scores/Best_score").getComponent(cc.Label);
    }


    updateUI(dt){
        // this.NameText.string = username from firebase.
        // this.scoreText.string = username from firebase.

        // this.timeText.string = username from firebase.
        // this.BestscoreText.string = username from firebase.

        // this.NameText.string = "Goro";
        // this.scoreText.string = "100";

        // this.timeText.string = "10 sec";
        // this.BestscoreText.string = "800";

    }



    // onLoad () {}


    Button_init(){
        let Startbtn = new cc.Component.EventHandler();
        Startbtn.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        Startbtn .component = "Victory";
        Startbtn .handler = "Restart_level_one";
        Startbtn .customEventData = "foobar";
        cc.find("Canvas/Background/Startbtn").getComponent(cc.Button).clickEvents.push(Startbtn);

        let Exitbtn = new cc.Component.EventHandler();
        Exitbtn.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        Exitbtn .component = "Victory";
        Exitbtn .handler = "EXIT";
        Exitbtn .customEventData = "foobar";
        cc.find("Canvas/Background/Exitbtn").getComponent(cc.Button).clickEvents.push(Exitbtn);

        let LDBbtn = new cc.Component.EventHandler();
        LDBbtn.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        LDBbtn .component = "Victory";
        LDBbtn .handler = "To_LDB";
        LDBbtn .customEventData = "foobar";
        cc.find("Canvas/LDBbtn").getComponent(cc.Button).clickEvents.push(LDBbtn);


    }
 
    Restart_level_one(event, customEventData) {
        setTimeout(()=>{
            cc.director.loadScene("level_1");
            return;
        }, 1000);        

    }

    EXIT(event, customEventData) {
        setTimeout(()=>{
            cc.director.loadScene("main");
            return;
        }, 1000);
    }
    To_LDB(event, customEventData) {
            cc.director.loadScene("LeaderBoard");
    }

    onLoad() {
        this.Button_init();
        this.initProperties();
    }

    // update (dt) {}


    playBGM(){
        // ===================== TODO =====================
        // 1. Play music. The audio clip to play is this.bgm
        cc.audioEngine.playMusic(this.bgm, true)
        // ================================================

    }



    start () {
        this.playBGM();


        // Get this time scores.
    
        var user = firebase.auth().currentUser;
        this.NameText.string = user.displayName + "'s ";

        firebase.database().ref('Users/'+user.displayName+'/num_of_times').once('value',(snapshot)=>{
            // get current scores.
            let num_of_times = snapshot.val();
            this.numText.string = String(num_of_times);
            firebase.database().ref('Users/'+user.displayName+'/'+num_of_times).once('value',(snapshot)=>{
                cc.log(snapshot.child('scores').val());
                this.scoreText.string = String(snapshot.child('scores').val());
                this.timeText.string = String(snapshot.child('time').val());
            })
        })

        firebase.database().ref('Users/'+user.displayName+'/Best_score').once('value',(snapshot)=>{
            this.BestscoreText.string  = String(snapshot.val());
        })



        
    }

    update (dt) {
        // if(!this.gameover){
            this.updateUI(dt);
            // if(this.remainTime == 0){
            //     this.gameover = true;
            //     this.onGameover();
            // }
        // }
    }
}

