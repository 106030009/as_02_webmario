// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class LeaderBoard extends cc.Component {

    // @property({type:cc.AudioClip})
    // bgm: cc.AudioClip = null;

    // LIFE-CYCLE CALLBACKS:
    // remain_lives: number = 0;
    name1Text: cc.Label;
    name2Text: cc.Label;
    name3Text: cc.Label;

    score1Text: cc.Label;
    score2Text: cc.Label;
    score3Text: cc.Label;    

    Button_Act(event, customEvent)
    {
        cc.director.loadScene("setting");

    }

   
    Button_Init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "LeaderBoard";
        button_Act1.handler = "Button_Act";
        button_Act1.customEventData = "Back to setting.";

        cc.find("Canvas/Backbtn").getComponent(cc.Button).clickEvents.push(button_Act1);

    }
    
    initProperties(){

        this.name1Text = cc.find("Canvas/LeaderBoard/Background/NO1/username").getComponent(cc.Label);
        this.name2Text = cc.find("Canvas/LeaderBoard/Background/NO2/username").getComponent(cc.Label);
        this.name3Text = cc.find("Canvas/LeaderBoard/Background/NO3/username").getComponent(cc.Label);
    
        this.score1Text = cc.find("Canvas/LeaderBoard/Background/NO1/username/score").getComponent(cc.Label);
        this.score2Text = cc.find("Canvas/LeaderBoard/Background/NO2/username/score").getComponent(cc.Label);
        this.score3Text = cc.find("Canvas/LeaderBoard/Background/NO3/username/score").getComponent(cc.Label);
    }

    onLoad () {
        this.initProperties();
        this.Draw();
    }

    start () {
        this.Button_Init();
        // this.Draw();
 
    }

    Draw(){
         // Go search in firebase.

        //  ByChild('Best_score')
        // OrderbyChild
        var userRef = firebase.database().ref('Users');
        userRef.orderByChild('Best_score').limitToFirst(3).once('value',(snapshot)=>{
            let i =1;
            snapshot.forEach((item)=>{
                console.log(item.child('Best_score').val());
                // cc.log(i);
                if(i==3){
                    this.name1Text.string = String(item.child('Name').val());
                    this.score1Text.string = String(item.child('Best_score').val());
                }else if(i==2){
                    this.name2Text.string = String(item.child('Name').val());
                    this.score2Text.string = String(item.child('Best_score').val());
                }else{
                    this.name3Text.string = String(item.child('Name').val());
                    this.score3Text.string = String(item.child('Best_score').val());
                }
                i++;
            })
        })

    }

    // update (dt) {}
}
