// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html



const {ccclass, property} = cc._decorator;

@ccclass
export default class Enemy_Turtle extends cc.Component {

    private beenhit: boolean = false;

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState
    
    private rebornPos = null;

    private isDead = true;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    start() {
        this.rebornPos = this.node.position;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
        this.isDead = false;
        this.animateState = this.anim.play('Turtle_anim');
    }

    update(dt) {
        if(this.isDead) {
            this.resetPos();
            this.isDead = false;
        }
    }

    public resetPos() {
        this.node.position = this.rebornPos;
        this.node.scaleX = 1;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
    }

    onBeginContact(contact, self, other) {
        if(other.node.name == "left_bound") {
            this.node.scaleX = -3.5;    // turn right.
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(100, 0);
        } else if(other.node.name == "right_bound") {
            this.node.scaleX = 3.5;
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
        } else if(other.node.name == "bullet") {
            this.isDead = true;
            other.node.destroy();
        }
        else if(other.tag == 3){ // block.
            if(this.beenhit){
                if(this.node.scaleX == -3.5){ //Mow moving right.
                    this.node.scaleX =  3.5; // Turn Left.
                   
                    this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0); // Move left.
                }
                else{
                    this.node.scaleX = -3.5;
                    this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);
                }
            }
            else{
                if(this.node.scaleX == -3.5){ //Mow moving right.
                    this.node.scaleX =  3.5; // Turn Left.
                    this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0); // Move left.
                }
                else{
                    this.node.scaleX = -3.5;
                    this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(100, 0);
                }
            }
        }else if(other.node.name =="mario" || other.node.name=="mario_level2"){
            if(this.beenhit){
                if(contact.getWorldManifold().normal.y ==1){
                    // stop speed.
                    this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0); // Move left.
                    this.anim.stop('Turtle_spin');
                }
                else{
                    // Just change direction.
                    this.animateState = this.anim.play('Turtle_spin');
                    if(other.node.x > this.node.x){ // mario is right of us.
                        this.node.scaleX =  3.5; // Turn Left.
                        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-200, 0); // Move left.
                    }
                    else{
                        this.node.scaleX = -3.5;
                        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(200, 0);    // Move right.
                    }
                }
            }
            else{
                if(contact.getWorldManifold().normal.y==1){ // Mario is on top of us.
                    this.anim.play('Turtle_shrink');
                    this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
                    this.beenhit = true;
                }
                else{
                    // Mario dead.
                }
            }
        }
    } // end onBeginContact.

}


