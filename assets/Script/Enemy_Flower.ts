// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Enemy_Folwer extends cc.Component {

    // speed : number = 40;

    state : string = "Up";
    lastState : string = "Up";

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    start () {
        this.animateState = this.anim.play('Flower_move');

        // Up , Pause, Down.
        this.schedule(function() {
            
            this.changeState();
            cc.log("Change state: " + this.state);

        }, 2);
    }
    changeState(){
        switch(this.state){
            case "Idle":
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);
                if(this.lastState == "Up"){
                    this.state = "Down";
                }
                else this.state = "Up";
            break;
            case "Up":
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,50);
                this.lastState = "Up";
                this.state = "Idle";
            break;

            case "Down":
                this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -50);
                this.lastState = "Down";
                this.state = "Idle";
            break;
        }

    }

    // update (dt) {
    // }
}
