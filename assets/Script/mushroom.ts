const {ccclass, property} = cc._decorator;

@ccclass
export default class mushroom extends cc.Component {


    // private jumpDuring:number = 3;
    // private jumpHeight:number = 30;

    private speed: number = 0;

    // setJumpAction(){
    //     var fadeIn   = cc.fadeIn(0);

    //     var jumpUp = cc.moveBy(this.jumpDuring, cc.v2(0, this.jumpHeight)).easing(cc.easeCubicActionOut());// 先快后慢


    //     return cc.sequence(fadeIn,jumpUp);
        
    // }
    
    onLoad(){
        cc.director.getPhysicsManager().enabled = true;  
        this.setInitDir();
    }

    private setInitDir(){
        
        var idx = Math.floor(Math.random() * 3 ) 

        if(idx > 0){
            this.node.scaleX = 3;
        }
        else{
            this.node.scaleX = -3;
        }

    }

    onBeginContact(contact, self, other){
        cc.log(this.node.scaleX);
        if(contact.getWorldManifold().normal.y != -1){

            if(other.tag == 3){  //block
                cc.log("Hit block.");
                if(this.node.scaleX>0) this.turnLeft();
                else this.turnRight();
            }
        }
    }
    
    update(dt){
        this.mushroomMove();

    }
   

    private mushroomMove(){

        if(this.node.scaleX > 0) {
            this.speed = 100; // the speed of mush
        } else {
            this.speed = -100; 
        }

        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0);

    }


    turnLeft(){
        this.node.scaleX = -3;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(-100, 0);
    }
    turnRight(){
        this.node.scaleX = 3;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(100, 0);
    }
    // ================================================
}
