// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Rules2 extends cc.Component {
    @property({type:cc.AudioClip})
    click: cc.AudioClip = null;

    Back(event, customEvent)
    {
        this.playEffect();
        cc.director.loadScene("setting");

    }
    Enter(event, customEvent)
    {
        this.playEffect();
        cc.director.loadScene("GameStart2");
    }

   
    Button_Init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "Rules2";
        button_Act1.handler = "Back";
        button_Act1.customEventData = "Sign up.";

        cc.find("Canvas/Backbtn").getComponent(cc.Button).clickEvents.push(button_Act1);

        let button_Act2 = new cc.Component.EventHandler();
        button_Act2.target = this.node;
        button_Act2.component = "Rules2";
        button_Act2.handler = "Enter";
        button_Act2.customEventData = "Log in.";

        cc.find("Canvas/Enterbtn").getComponent(cc.Button).clickEvents.push(button_Act2);



    }


    playEffect(){
        // ===================== TODO =====================
        // 1. Play sound effect. The audio clip to play is 
        cc.log("Click.");
        cc.audioEngine.playEffect(this.click, false);
        // ================================================
    }

    // onLoad () {}

    start () {
        this.stopBGM();
        this.Button_Init();
    }
    // onLoad () {}
    stopBGM(){
        // ===================== TODO =====================
        // 1. Stop music. 
        cc.audioEngine.pauseMusic();
        // ================================================
    }

    // update (dt) {}
}
