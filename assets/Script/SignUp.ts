// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SignUp extends cc.Component {

    NameText : cc.EditBox;
    EmailText: cc.EditBox;
    passwordText: cc.EditBox;

    Button_Act(event, customEvent)
    {

        if(this.EmailText!=null && this.passwordText !=null && this.NameText!=null){
              
            firebase.auth().createUserWithEmailAndPassword(this.EmailText.string, this.passwordText.string).then( (user)=> {
                // successfully create account.    
                    // cc.director.loadScene("setting");

                    firebase.auth().onAuthStateChanged( (user)=> {
                        if (user) {
                            var user = firebase.auth().currentUser;
                            // alert(this.NameText.string);  

                            user.updateProfile({
                                displayName: this.NameText.string
                            }).then(()=> {    
                                alert(user.displayName);                                         
                                this.logUser(user.displayName); // Optional
                                //Initialize life.
                                firebase.database().ref('Users/'+user.displayName+'/').update({
                                    Lives: 3
                                })
                                cc.director.loadScene("select_mode");
                                    // Update successful.
                            }).catch(function(error) {
                                      alert(error);
                                    // An error happened.
                            });
                        } else {
                            alert("User is null.");
                          // No user is signed in.
                        }
                      });






                // alert(user.email);  
                        // var user = firebase.auth().currentUser;
                        // firebase.database().ref("Users").push({
                        //     "user": this.NameText.string,
                        //     "email": user.email
                        // });
            }, function(error) {
                    alert(error);
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                });

        }

    }

   
    Back(event, customEvent){
        cc.director.loadScene("main");
    }
    Button_Init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "SignUp";
        button_Act1.handler = "Button_Act";
        button_Act1.customEventData = "Sign up.";

        cc.find("Canvas/SignUpContainer/SignUpbtn").getComponent(cc.Button).clickEvents.push(button_Act1);

        let button_Act2 = new cc.Component.EventHandler();
        button_Act2.target = this.node;
        button_Act2.component = "SignUp";
        button_Act2.handler = "Back";
        button_Act2.customEventData = "Back.";

        cc.find("Canvas/Backbtn").getComponent(cc.Button).clickEvents.push(button_Act2);
    }


    init_text(){
        this.NameText = cc.find("Canvas/SignUpContainer/Username").getComponent(cc.EditBox);
        this.EmailText = cc.find("Canvas/SignUpContainer/Email").getComponent(cc.EditBox);
        this.passwordText = cc.find("Canvas/SignUpContainer/Password").getComponent(cc.EditBox);
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        this.init_text();
    }

    start () {
        // init logic
        this.Button_Init();
        // this.label.string = this.text;
    }

    update(dt){

    }


    logUser(username) {

        // firebase.database().ref('Users').set({
        //     Name: username
        // });
        firebase.database().ref('Users/'+ username).set({
            Name: username,
            num_of_times: 0,
            Best_score: 0,
            isBig: 0
        });

        // var ref = firebase.database().ref("Users");
        // var obj = {
        //     "Name": username
        // };
        // ref.push(obj); // or however you wish to update the node
    }

}
