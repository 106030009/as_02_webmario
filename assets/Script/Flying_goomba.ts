// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html



const {ccclass, property} = cc._decorator;

@ccclass
export default class Flying_goomba extends cc.Component {


    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState
    
    private rebornPos = null;

    private isDead = false;

    private speed = 100;

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        this.anim = this.getComponent(cc.Animation);
    }

    start() {
        this.rebornPos = this.node.position;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0);
        this.isDead = false;
    }

    update(dt) {
        if(this.isDead) {
            this.resetPos();
            this.isDead = false;
        }else{
            this.Move();
        }
    }

    Move(){
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0);
    }


    public resetPos() {
        cc.log(this.rebornPos);
        this.node.position = this.rebornPos;
   //     this.node.scaleX = 3;
        this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.speed, 0);
    }

    onBeginContact(contact, self, other) {
        if(other.node.name == "left_bound" || "right_bound") {
            this.speed = -this.speed;
        }else if(other.tag == 3){ // block.
            if(contact.getWorldManifold().normal.x==1 || contact.getWorldManifold().normal.x==-1){   // Flying_goomba is not STANDING on it.
                 this.speed = -this.speed;
                 cc.log("Flying contact: "+other.node.name);
            }
        }else if(other.node.name =="mario"  || other.node.name=="mario_level2"){
            // mario on top.
            if(contact.getWorldManifold().normal.y==1){
                cc.log("Flying_dead");
                this.animateState = this.anim.play('goomba_shrink');
      //          this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);     
       //         this.scheduleOnce( ()=>{
                    this.isDead = true;
            //        this.node.destroy();
      //          }, 0.2);
            }
        }
    } // end onBeginContact.



}


