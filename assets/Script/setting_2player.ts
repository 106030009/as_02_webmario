// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class setting_2player extends cc.Component {


///////// TODO : New a 2player level_1 scene ////////////
/// create two players. //////////


    To_Level_1(event, customEvent){
        cc.log(customEvent);
        cc.director.loadScene("Rules_2p");

    }
    
    To_Level_2(event, customEvent){
        cc.log(customEvent);
        alert("Sorry! This level is still being developed!");
   //     cc.director.loadScene("Rules2");

    }
    Back(event, customEvent){
        //   cc.log(customEvent);
           cc.director.loadScene("select_mode");
   
       }


    Button_init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "setting_2player";
        button_Act1.handler = "To_Level_1";
        button_Act1.customEventData = "Load level one.";

        cc.find("Canvas/Level1btn").getComponent(cc.Button).clickEvents.push(button_Act1);

        let button_Act2 = new cc.Component.EventHandler();
        button_Act2.target = this.node;
        button_Act2.component = "setting_2player";
        button_Act2.handler = "To_Level_2";
        button_Act2.customEventData = "Load level two.";

        cc.find("Canvas/Level2btn").getComponent(cc.Button).clickEvents.push(button_Act2);

        let button_Act3 = new cc.Component.EventHandler();
        button_Act3.target = this.node;
        button_Act3.component = "setting_2player";
        button_Act3.handler = "Back";
        button_Act3.customEventData = "Back";

        cc.find("Canvas/Backbtn").getComponent(cc.Button).clickEvents.push(button_Act3);


    }


    // onLoad () {}
    stopBGM(){
        // ===================== TODO =====================
        // 1. Stop music. 
        cc.audioEngine.pauseMusic();
        // ================================================
    }
    start () {
        // this.stopBGM();
        this.Button_init();
    }

    // update (dt) {}
}
