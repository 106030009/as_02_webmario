// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class boat extends cc.Component {

    private type: string = null;

    // LIFE-CYCLE CALLBACKS:

    randomGenDir()
    {
        let rand = Math.random();

        //0: 'h', 1: 'v'
        let prob = [1,1];
        let sum = prob.reduce((a,b)=>a+b);

        for(let i = 1; i < prob.length; i++)
            prob[i] += prob[i-1];
        for(let i = 0; i < prob.length; i++)
        {
            prob[i] /= sum;
            if(rand <= prob[i])
                return i;
        }
    }


    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        if(this.randomGenDir()==0){
            this.type = 'h';
        }else{
            this.type = 'v';
        }
        //random generate direction.
    }

    start () {
        let easeRate: number = 2;
        // run action.
        if(this.type=="h"){//move horizontally.        
               // horizontal move.
               let mov_r = cc.moveBy(2,50,0);
               let mov_l = cc.moveBy(2,-50,0);
               mov_r.easing(cc.easeInOut(easeRate));
               mov_l.easing(cc.easeInOut(easeRate));
   
               let action = cc.repeatForever(
                   cc.sequence(mov_r,mov_l)
               )
               this.node.runAction(action);    
        }else if(this.type=="v"){   //move vertically.

            let mov_u = cc.moveBy(2,0,50);
            let mov_d = cc.moveBy(2,0,-50);
            mov_u.easing(cc.easeInOut(easeRate));
            mov_d.easing(cc.easeInOut(easeRate));

            let action = cc.repeatForever(
                cc.sequence(mov_u,mov_d)
            )
            this.node.runAction(action);
        }
    

    }

    onBeginContact(contact, self, other) {{
        // Mario on Top.
        if(contact.getWorldManifold().normal.y== 1){
            let boat = this.getComponent(cc.RigidBody);
            if(other.node.name == "mario"){
                cc.log("Boat pos : "+ this.node.x + ","+ this.node.y + " Mario speed: "+ other.node.x+", "+ other.node.y);
                if(this.type == "h"){
                    cc.log("Boat v : "+ boat.linearVelocity + " Mario speed: "+ other.getComponent(cc.RigidBody).linearVelocity);
                    // let boat = this.getComponent(cc.RigidBody);
                    other.getComponent(cc.RigidBody).linearVelocity = cc.v2(boat.linearVelocity.x, boat.linearVelocity.y);
                }else{               
                    other.getComponent(cc.RigidBody).linearVelocity = cc.v2(boat.linearVelocity.x, boat.linearVelocity.y);
                }

                
               // other.getComponent(cc.RigidBody).linearVelocity = cc.v2()
            }

        }
    }


    // update (dt) {}
}
