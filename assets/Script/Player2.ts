
const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    @property(cc.SpriteFrame)
    BigMarioSprite: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    smallMarioSprite: cc.SpriteFrame = null;

    @property({type:cc.AudioClip})
    GrowSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    ShrinkSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    hurtSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    dieSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    jumpSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    kickSound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    stompSound: cc.AudioClip = null;   

    @property({type:cc.AudioClip})
    MushroomUp: cc.AudioClip = null;   

    
    @property({type:cc.AudioClip})
    coinSound: cc.AudioClip = null;       

    @property({type:cc.AudioClip})
    gameoverSound: cc.AudioClip = null; 


    @property({type: cc.Node})
    Canvas: cc.Node = null;
    
    @property({type: cc.Node})
    mainCamera: cc.Node = null;

    @property({type: cc.Node})
    Mario1: cc.Node = null;



    @property({type: cc.Node})
    Qblocks: cc.Node[] = [];

    private hitQblocks :boolean[] = [false,false,false,false,false,false,false,false];

    @property([cc.Prefab])
    ItemPrefabs: cc.Prefab[] = [];
    // 0:M 1:coin


    // private idleFrame: cc.SpriteFrame = null;

    // private anim: cc.Animation = null;
    private rebornPos = null;
    
    isBig: boolean = false; //defaule: small mario.

    lives: number = null;

    // private ceilingPos: number = 155;

    // private fallDown: boolean = false;

    private damageTime: number = 0;

    private growing: boolean = false;
    
    private shrinking: boolean = false;

    private dieing: boolean = false;
    

    @property(cc.Node)
    lifeContainer: cc.Node = null;

    @property(cc.Prefab)
    lifePrefab: cc.Prefab = null;

    coinText : cc.Label = null;
    scoreText : cc.Label = null;
    score : number = 0;
    coins : number = 0;

    private anim = null; //this will use to get animation component
    
    private anim_str: string = null;


    private animateState = null; //this will use to record animationState

    // private bulletPool = null; // this is a bullet manager, and it control the bullet resource

    private playerSpeed: number = 0;



    // LIFE-CYCLE CALLBACKS:


    // @property(cc.Prefab)
    // private bulletPrefab: cc.Prefab = null;


    private LDown: boolean = false; // key for player to go left

    private RDown: boolean = false; // key for player to go right

    // private jDown: boolean = false; // key for player to shoot

    private SpaceDown: boolean = false; // key for player to jump

    private isDead: boolean = false;

    private onGround: boolean = false;



    onLoad() {
        cc.director.getPhysicsManager().enabled = true;     	
        // this.idleFrame = this.getComponent(cc.Sprite).spriteFrame;
        this.anim = this.getComponent(cc.Animation);
        this.lives = 3; //initiall lives.

            for(let i=1; i<=this.lives; i++){
                var newlife = cc.instantiate(this.lifePrefab);
                newlife.name = "Life"+String(i);
                cc.log(newlife.name);
                this.lifeContainer.addChild(newlife);
                newlife.scaleX = 0.3;
                newlife.scaleY = 0.3;
                newlife.setPosition(50 +30*(i-1) , 0);
            }

    }

    start(){
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.coinText = cc.find("Canvas/Main Camera/level1/TopContent/Coins2/number").getComponent(cc.Label);
        this.scoreText = cc.find("Canvas/Main Camera/level1/TopContent/Score2/value").getComponent(cc.Label);
     //   this.animateState = this.anim.play('idle'); // Initial animation
        this.anim.play('idle');
      //      this.playerAnimation('idle');
        //     this.anim.play('idle');
        this.rebornPos = this.node.position;

        // Reset Q blocks to false.
        for(let k=0; k<=7; k++){
            this.hitQblocks[k] = false;
        }

        // this.level_1.node.getComponent('level_1').remain_lives = 3;
        // this.level_1.getComponent("level_1").updateLife(this.lives);
    }

    onKeyDown(event) {
        // cc.log("Key Down: " + event.keyCode);
        if(event.keyCode == cc.macro.KEY.z) {
             if(this.isBig)this.anim.play('Big_move');
             else this.anim.play('move');
            this.LDown = true;
            this.RDown = false;
        } else if(event.keyCode == cc.macro.KEY.c) {
             if(this.isBig)this.anim.play('Big_move');
             else this.anim.play('move');

            this.RDown = true;
            this.LDown = false;
        } else if(event.keyCode == cc.macro.KEY.a) {
            ///////// TODO : BIG-JUMP animation. //////////
             if(this.isBig)this.anim.play('Big_jump');
             else this.anim.play('mario_jump');
            this.SpaceDown = true;
        } 

    }
    
    onKeyUp(event) {
        if(event.keyCode == cc.macro.KEY.z){
            // this.animateState = this.anim.stop('move');
             if(this.isBig)  this.anim.play('Big_idle');
             else this.anim.play('idle');            
            
            this.LDown = false;
        }
        else if(event.keyCode == cc.macro.KEY.c){
            if(this.isBig)  this.anim.play('Big_idle');
            else this.anim.play('idle');  
            this.RDown = false;
        }
        else if(event.keyCode == cc.macro.KEY.a)
   //     if(this.isBig)  this.anim.play('Big_idle');
    //    else this.anim.play('idle');  
            this.SpaceDown = false;
    }
    
    private playerMovement(dt) {
        this.playerSpeed = 0;
        if(this.isDead) {
            cc.log('Player2 die.');
            this.LDown = false;
            this.RDown = false;
            this.SpaceDown = false;
            this.lives -- ;
            this.anim.play('mario_die');
            this.node.position = this.rebornPos;       

            this.isDead = false;
            this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 1500);
            this.node.getComponent(cc.PhysicsBoxCollider).enabled = false; // Hide this node avoid multiple contact(DEAD).
    //        this.playerAnimation('mario_die');
            this.anim.play('mario_die');
            this.lifeContainer.getChildByName("Life"+String(this.lives+1)).destroy();
            // CANNOT control player anymore~
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
            cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
            
            if(this.lives <= 0){
          //      cc.audioEngine.pauseMusic();
       //         cc.audioEngine.playEffect(this.gameoverSound,false);
                this.node.position = this.node.position;
                this.mainCamera.x = this.node.x;    // Don't move!!
                // re gain lives
/*
                firebase.database().ref('Users/'+user.displayName+'/').update({
                    Lives:  3
                });*/

                setTimeout(()=>{
       //             this.node.position = this.rebornPos;
                    // this.node.getComponent(cc.RigidBody).enabled = true;
        //            cc.director.loadScene("Gameover");
                    return;
                }, 4000);
            }else{
                //  Play die animation.
                // Stop bgm.
                cc.audioEngine.pauseMusic();

                cc.audioEngine.playEffect(this.dieSound,false);
                // this.node.getComponent(cc.PhysicsBoxCollider).enabled = false; // Hide this node avoid multiple contact(DEAD).

                setTimeout(()=>{
                        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
                        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
                        this.anim.play('idle');
                        this.node.position = this.rebornPos;
                        this.node.getComponent(cc.PhysicsBoxCollider).enabled =  true;
                        cc.audioEngine.resumeMusic();
                        return;
                }, 2000);
            }
        }else{

            // Still alive.
            if(this.LDown){
                this.playerSpeed = -300;
                this.node.scaleX = -3.5;
            }
            else if(this.RDown){
                this.playerSpeed = 300;
                this.node.scaleX = 3.5;
            }else{  // idle
            }
            
            this.node.x += this.playerSpeed * dt;
            if(this.SpaceDown && this.onGround){
                this.jump();
            }
        }
    }    

    private jump() {
        // cc.log("Press Jump.");
        this.onGround = false;
        cc.audioEngine.playEffect(this.jumpSound,false);
        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 1500);
    }

    
    update(dt) {

        if(this.node.x < (this.mainCamera.x - this.Canvas.width/2) || this.node.x > (this.mainCamera.x + this.Canvas.width/2)  ){
            this.isDead = true;
        }
        this.playerMovement(dt);
    //    this.mainCamera.x = this.node.x;
        this.coinText.string = this.coins.toString();
        this.scoreText.string = this.score.toString();
        this.rebornPos = cc.v2(this.Mario1.x+100 , this.Mario1.y);

    }


    // Callback functions for the beginning of contact.
    onBeginContact(contact, self, other) {

        // Mario on Top.
          if(contact.getWorldManifold().normal.y==-1){
            if(other.node.name == "ground") {
                this.onGround = true;
            } else if(other.tag == 2) { // tag = 2: enemy.
                cc.log("Enemy die.");
                cc.audioEngine.playEffect(this.stompSound, false);
                if(other.node.name == "Enemy_Turtle"){
                    if(other.node.getComponent('Enemy_Turtle').beenhit == true){
                        this.onGround = true; // mario can jump.
                        other.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,0);
                    }
                    else{
                        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,500);
                        // Turtle would shrink in its TypeScript.
                    }
                }else if(other.node.name =="Enemy_goomba"){
                    // goomba destroy in its TypeScript.
                }else{
                    other.node.destroy();
                }
            }
            else if(other.tag == 3 || other.tag==4 || other.tag==7) {   // any block or Qblock.
                this.onGround = true;
            }
            else if(other.node.name == "left_bound" || other.node.name == "right_bound" || other.node.name == "Lower_bound"){
                this.isDead = true;
            }else if(other.node.name =="coin"){
                cc.audioEngine.playEffect(this.coinSound,false);
                this.coins += 1;
                other.node.destroy();
            }else if(other.tag == 5){ 
                // grow up mushroom.
                cc.log("Eat mushroom.");
                if(!this.isBig){//can grow up
                    // First update state to firebase.
           /*         var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        isBig : 1
                    }).then(()=>{*/
                        this.isBig = true;
                        this.growing = true;
                        this.anim.play('mario_grow');
                        // this.playerAnimation('mario_grow');
               //         this.animateState = this.anim.play('mario_grow');
                        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,200);
                        this.changeSprite(this.BigMarioSprite);              
                        cc.audioEngine.playEffect(this.GrowSound, false);                       
             //       })
                }else{
                    cc.log("Already big!");
                    cc.audioEngine.playEffect(this.coinSound, false);
                }
                other.node.destroy();
            
                // this.get bigger.
                
                // this.sprite.frame = ;
            }else if(other.node.name == "Lives"){   
                if(this.lives >=3){
                    this.lives = 3; //maintain.
                    cc.log("Life saturated.");
                    other.node.destroy();
                }
                else{
                    this.lives ++;
                    cc.log("Lives: " +this.lives);
                    other.node.destroy();
                    //update to firebase
                    /*
                    var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        Lives: this.lives
                    });*/

                    // Draw UI Life :
                    var newlife = cc.instantiate(this.lifePrefab);
                    this.lifeContainer.addChild(newlife);
                    newlife.scaleX = 0.3;
                    newlife.scaleY = 0.3;

                    if(this.lives == 3){
                        newlife.name = "Life3"
                        newlife.setPosition(50 +30*2 , 0);
                    }else if(this.lives ==2){
                        newlife.name = "Life2";
                        newlife.setPosition(50 +30*1 , 0);
                    }else{
                        newlife.name = "Life1";
                        newlife.setPosition(50 +30*0 , 0);
                    }
                }
            }
          }


          // Mario at Bottom.
          else if(contact.getWorldManifold().normal.y== 1){
            if(other.node.name == "ground") {
            } else if(other.tag == 2) { // tag = 2: enemy.
                cc.log("Enemy die.");
                // this.isDead = true;
            }
            else if(other.tag == 3 || other.tag==7) {
            }else if(other.tag == 4) {  // Qblock.
                cc.log("Mario hits the question box.");

                    switch(other.node.name){
                        case "Qblock0":
                            if(!this.hitQblocks[0]){
                                this.hitQblocks[0] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[0].addChild(item);
                                item.setPosition(0,this.Qblocks[0].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;
                        case "Qblock1":
                            if(!this.hitQblocks[1]){
                                this.hitQblocks[1] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[1].addChild(item);
                                item.setPosition(0,this.Qblocks[1].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;
                        case "Qblock2":
                            if(!this.hitQblocks[2]){
                                this.hitQblocks[2] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[2].addChild(item);
                                item.setPosition(0,this.Qblocks[2].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;       
                        case "Qblock3":
                            if(!this.hitQblocks[3]){
                                this.hitQblocks[3] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[3].addChild(item);
                                item.setPosition(0,this.Qblocks[3].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;      
                        case "Qblock4":
                            if(!this.hitQblocks[4]){
                                this.hitQblocks[4] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[4].addChild(item);
                                item.setPosition(0,this.Qblocks[4].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;
                        case "Qblock5":
                            if(!this.hitQblocks[5]){
                                this.hitQblocks[5] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[5].addChild(item);
                                item.setPosition(0,this.Qblocks[5].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;
                        case "Qblock6":
                            if(!this.hitQblocks[6]){
                                this.hitQblocks[6] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[6].addChild(item);
                                item.setPosition(0,this.Qblocks[6].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;       
                        case "Qblock7":
                            if(!this.hitQblocks[7]){
                                this.hitQblocks[7] = true;
                                let idx = this.randomGenItem();
                                let item = cc.instantiate(this.ItemPrefabs[idx]);   // item : cc.Node
                                this.Qblocks[7].addChild(item);
                                item.setPosition(0,this.Qblocks[7].height);
                                if(idx==0){
                                    cc.audioEngine.playEffect(this.MushroomUp,false);
                                }else if(idx==1){
                                    cc.audioEngine.playEffect(this.coinSound,false);
                                }else{
                                    // Generate live.
                                    cc.audioEngine.playEffect(this.GrowSound,false);
                                }
                            }else{
                                cc.log(other.node.name + " has been hit.");
                            }
                        break;                                                            
                    }
                
            }else if(other.node.name =="coin"){
                this.coins += 1;
                cc.audioEngine.playEffect(this.coinSound,false);
                other.node.destroy();
            }else if(other.tag == 5){ 
                // grow up mushroom.
                cc.log("Eat mushroom.");
                if(!this.isBig){//can grow up
                    // First update state to firebase.
             /*       var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        isBig : 1
                    }).then(()=>{*/
                        this.isBig = true;
                        this.growing = true;
                        this.anim.play('mario_grow');
             //           this.playerAnimation('mario_grow');

                        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,200);
                        this.changeSprite(this.BigMarioSprite);              
                        cc.audioEngine.playEffect(this.GrowSound, false);                       
               //     })


                    // this.animateState = this.anim.play('mario_grow');
                    // this.isBig = true;
                    // this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,200);
                    // this.changeSprite(this.BigMarioSprite);              
                    // cc.audioEngine.playEffect(this.GrowSound, false);
                }else{
                    cc.log("Already big!");
                    cc.audioEngine.playEffect(this.coinSound, false);
                }
                other.node.destroy();
            
                // this.get bigger.
                
                // this.sprite.frame = ;
            }else if(other.node.name == "Lives"){   
                if(this.lives >=3){
                    this.lives = 3; //maintain.
                    cc.log("Life saturated.");
                    other.node.destroy();
                }
                else{
                    this.lives ++;
                    cc.log("Lives: " +this.lives);
                    other.node.destroy();
                    //update to firebase

                  /*  
                    var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        Lives: this.lives
                    });*/

                    // Draw UI Life :
                    var newlife = cc.instantiate(this.lifePrefab);
                    this.lifeContainer.addChild(newlife);
                    newlife.scaleX = 0.3;
                    newlife.scaleY = 0.3;

                    if(this.lives == 3){
                        newlife.name = "Life3"
                        newlife.setPosition(50 +30*2 , 0);
                    }else if(this.lives ==2){
                        newlife.name = "Life2";
                        newlife.setPosition(50 +30*1 , 0);
                    }else{
                        newlife.name = "Life1";
                        newlife.setPosition(50 +30*0 , 0);
                    }
                }
            }
          }





          // Left or Right direction.
          else{
            if(other.node.name == "ground") {
                this.onGround = true;
            }             
            else if(other.tag == 2) { // tag = 2: enemy.
                if(other.node.name == "Enemy_Turtle"){
                    if(other.node.getComponent('Enemy_Turtle').beenhit == true){
                        cc.audioEngine.playEffect(this.kickSound,false);
                    }
                    else{
                        if(this.isBig){//Should shrink (won't die)

                        // First update state to firebase.
                        /*
                        var user = firebase.auth().currentUser;
                        firebase.database().ref('Users/'+user.displayName+'/').update({
                            isBig : 0
                        }).then(()=>{*/
                            this.isBig = false;
                            this.shrinking = true;
                            this.anim.play('mario_shrink');
                            // this.playerAnimation('mario_shrink');
                            this.changeSprite(this.smallMarioSprite);              
                            cc.audioEngine.playEffect(this.hurtSound, false);                   
                   //     })
                        }else{                    
                            cc.audioEngine.playEffect(this.hurtSound,false);
                            this.isDead = true;
                        }
                    }
                }
                else{
                    if(this.isBig){//Should shrink (won't die)

                    // First update state to firebase.
                    /*
                    var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        isBig : 0
                    }).then(()=>{*/
                        this.isBig = false;
                        this.shrinking = true;
                        this.anim.play('mario_shrink');
                        // this.playerAnimation('mario_shrink');
                        this.changeSprite(this.smallMarioSprite);              
                        cc.audioEngine.playEffect(this.hurtSound, false);                   
               //     })

                    }else{                    
                        this.isDead = true; 
                    }
                }
            }
            else if(other.tag == 3 || other.tag==4) {
                this.onGround = true;
            }
            else if(other.tag == 5){ 
                // grow up mushroom.
                cc.log("Eat mushroom.");
                if(!this.isBig){//can grow up
                    // First update state to firebase./
                    /*
                    var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        isBig : 1
                    }).then(()=>{*/
                        this.isBig = true;
                        this.growing = true;
                        this.anim.play('mario_grow');
                        // this.playerAnimation('mario_grow');
                        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0,200);
                        this.changeSprite(this.BigMarioSprite);              
                        cc.audioEngine.playEffect(this.GrowSound, false);               
             
             //       })
                }else{
                    cc.log("Already big!");
                    cc.audioEngine.playEffect(this.coinSound, false);
                }
                other.node.destroy();
            
                // this.get bigger.
                
                // this.sprite.frame = ;
            }
            else if(other.node.name == "left_bound" || other.node.name == "right_bound" || other.node.name == "Lower_bound"){
                this.isDead = true;
            }
            else if(other.tag == 6){//reach goal flag.
                cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
                cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
                contact.disabled = true;
                this.LDown = this.RDown = this.SpaceDown = false;
                //this.playerSpeed = 0;
                let action = cc.repeatForever(cc.moveBy(1, 0, 1500))
                cc.log("Run action now.");

                this.node.runAction(action);
        //        this.playerAnimation('mario_win');
                this.animateState = this.anim.play('mario_win');
                // Send "win" to level_1.ts.
                let Rtime = cc.find("Canvas").getComponent("level_2").remainTime;
                this.score = Rtime.toFixed(0).toString().replace(".", ":");
               cc.find("Canvas").getComponent("level_2").win();

    

            }else if(other.node.name =="coin"){
                this.coins += 1;
                cc.audioEngine.playEffect(this.coinSound,false);
                other.node.destroy();
            }else if(other.node.name == "Lives"){
                if(this.lives >=3){
                    this.lives = 3; //maintain.
                    cc.log("Life saturated.");
                    other.node.destroy();
                }
                else{
                    this.lives ++;
                    cc.log("Lives: " +this.lives);
                    other.node.destroy();
                    //update to firebase
                    /*
                    var user = firebase.auth().currentUser;
                    firebase.database().ref('Users/'+user.displayName+'/').update({
                        Lives: this.lives
                    });*/

                    // Draw UI Life :
                    var newlife = cc.instantiate(this.lifePrefab);
                    this.lifeContainer.addChild(newlife);
                    newlife.scaleX = 0.3;
                    newlife.scaleY = 0.3;

                    if(this.lives == 3){
                        newlife.name = "Life3"
                        newlife.setPosition(50 +30*2 , 0);
                    }else if(this.lives ==2){
                        newlife.name = "Life2";
                        newlife.setPosition(50 +30*1 , 0);
                    }else{
                        newlife.name = "Life1";
                        newlife.setPosition(50 +30*0 , 0);
                    }
                }
            }
          }

    }    // End onBeginContact.



    private playerAnimation(anim_str: string)
    {
        var playing = this.anim.play(this.anim_str).isPlaying;

        // Grow, shrink, die need to be frozen. CANNOT change to idle
        if(anim_str == this.anim_str)return;
        else{ 
                cc.log("Change amim!");
                cc.log("Now: "+this.anim_str+", Play: "+anim_str);
               this.anim.stop();
                this.anim_str = anim_str;
                this.anim.play(this.anim_str);                
        }
    }

    
    randomGenItem()
    {
        let rand = Math.random();

        //0: Mushroom, 1: coin, 2: lives.
        let prob = [1,1,1];
        let sum = prob.reduce((a,b)=>a+b);

        for(let i = 1; i < prob.length; i++)
            prob[i] += prob[i-1];
        for(let i = 0; i < prob.length; i++)
        {
            prob[i] /= sum;
            if(rand <= prob[i])
                return i;
        }
    }

    changeSprite(Sprite: cc.SpriteFrame) {
        // change sprite frame to the one specified by the property
        this.getComponent(cc.Sprite).spriteFrame = Sprite;
    }


        


}
