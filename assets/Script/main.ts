
    
 const {ccclass, property} = cc._decorator;


@ccclass
export default class main extends cc.Component {

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    click: cc.AudioClip = null;

    @property
    text: string = 'hello';

    Button_Act(event, customEvent)
    {
        this.playEffect();
        cc.director.loadScene("SignUp");

    }
    Button_Act2(event, customEvent)
    {
        this.playEffect();
        cc.director.loadScene("LogIn");
    }

   
    Button_Init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "main";
        button_Act1.handler = "Button_Act";
        button_Act1.customEventData = "Sign up.";

        cc.find("Canvas/SignUpbtn").getComponent(cc.Button).clickEvents.push(button_Act1);

        let button_Act2 = new cc.Component.EventHandler();
        button_Act2.target = this.node;
        button_Act2.component = "main";
        button_Act2.handler = "Button_Act2";
        button_Act2.customEventData = "Log in.";

        cc.find("Canvas/LogInbtn").getComponent(cc.Button).clickEvents.push(button_Act2);



    }

    playBGM(){
        // ===================== TODO =====================
        // 1. Play music. The audio clip to play is this.bgm
        cc.audioEngine.playMusic(this.bgm, true)
        // ================================================

    }

    // stopBGM(){
    //     // ===================== TODO =====================
    //     // 1. Stop music. 
    //     cc.audioEngine.pauseMusic();
    //     // ================================================
    // }

    playEffect(){
        // ===================== TODO =====================
        // 1. Play sound effect. The audio clip to play is 
        cc.log("Click.");
        cc.audioEngine.playEffect(this.click, false);
        // ================================================
    }


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.playBGM();
    }

    start () {
        // init logic
        this.playBGM();
        this.Button_Init();
        // this.label.string = this.text;
    }
}

