// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class LogIn extends cc.Component {
 



    EmailText: cc.EditBox;
    passwordText: cc.EditBox;

    Button_Act(event, customEvent)
    {
        if(this.EmailText!=null && this.passwordText !=null){
            firebase.auth().signInWithEmailAndPassword(this.EmailText.string, this.passwordText.string).then((user)=> {
                            // If successfully sign in.


                  cc.director.loadScene("select_mode");
                
            }).catch(function(error) {
                alert(error);
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            });
        }
    }

    Back(event, customEvent){
        cc.director.loadScene("main");
    }

   
    Button_Init(){
        let button_Act1 = new cc.Component.EventHandler();
        button_Act1.target = this.node;
        button_Act1.component = "LogIn";
        button_Act1.handler = "Button_Act";
        button_Act1.customEventData = "LogIn.";

        cc.find("Canvas/LogInContainer/LogInbtn").getComponent(cc.Button).clickEvents.push(button_Act1);

        let button_Act2 = new cc.Component.EventHandler();
        button_Act2.target = this.node;
        button_Act2.component = "LogIn";
        button_Act2.handler = "Back";
        button_Act2.customEventData = "Back.";

        cc.find("Canvas/Backbtn").getComponent(cc.Button).clickEvents.push(button_Act2);

    }

    init_text(){
        this.EmailText = cc.find("Canvas/LogInContainer/Email").getComponent(cc.EditBox);
        this.passwordText = cc.find("Canvas/LogInContainer/Password").getComponent(cc.EditBox);
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad () {}

    start () {
        // init logic
        this.Button_Init();
        this.init_text();
        // this.label.string = this.text;
    }

    update(dt){

    }


}